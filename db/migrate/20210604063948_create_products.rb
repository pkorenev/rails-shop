class CreateProducts < ActiveRecord::Migration[6.1]
  def change
    create_table :products do |t|
      t.string :name, null: false
      t.string :sku, null: false
      t.decimal :price, null: false
      t.text :full_description
      t.jsonb :properties

      t.timestamps
    end
  end
end
