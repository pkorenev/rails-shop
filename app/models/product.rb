class Product < ApplicationRecord
  validates :name, :sku, :price, presence: true
end
